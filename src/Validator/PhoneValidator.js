class PhoneValidator {

    static validator(value) {
        return PhoneValidator.mask(value);
    }

    static mask(value) {
        let x = value
                    .replace(/\D/g, '')
                    .match(/(\d{0,2})(\d{0,1})(\d{0,4})(\d{0,4})/);
                    
        value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + '.' + x[3] + (x[4] ? '-' + x[4] : '');

        return value;
    }
}

export default PhoneValidator;