import React from "react";
import "./login.css";

import Field from "./../../Components/Field";
import Notify from "./../../Components/Notify";

class Login extends React.Component {

    constructor(props) {

        super(props);

        this.state = {
            form: {
                username: "",
                password: "",
            },
            errors: []
        }

    }

    componentDidMount(){
        this.isLogged();
    }

    isLogged(){
        let logged = localStorage.getItem('logged');
        logged = JSON.parse(logged);

        if(logged !== null){
            this.props.history.push('/system');
        }else{
            return false;
        }
    }

    setFieldValue(id, value){
        let form = this.state.form;
        form[id] = value;
        this.setState({ form });
    }

    throwErrors(errors) {
        this.setState({ errors }, () => {
            setTimeout(() => {
                this.setState({ errors: [] });
            }, 5000);
        });
    }

    handleSubmit() {
        let { username, password } = this.state.form;
        let errors = [];

        if (username.length < 1) {
            errors.push(`Username field is mandatory`);
        }

        if (password.length < 1) {
            errors.push(`Password field is mandatory`);
        }

        if (errors.length > 0) {
            return this.throwErrors(errors);
        }

        let users = localStorage.getItem('users');
        let logged = null;

        let checker = JSON.parse(users).find((user) => {
            logged = user;
            return (user.username === username && user.password === password);
        });

        if(!checker){
            errors.push(`User not found.`);
            return this.throwErrors(errors);
        }

        localStorage.setItem('logged', JSON.stringify(logged));

        this.props.history.push('/system');
    }

    render() {
        return (
            <div id="login">
                <div className="left">
                    <div className="container">
                        <h1>LOGIN</h1>
                        <h3>
                            The best contact <br />
                            manager system <br />
                            on the web
                        </h3>
                    </div>
                </div>
                <div className="right">
                    <Notify errors={this.state.errors} />
                    <div className="squares">
                        <div className="square"></div>
                        <div className="square default"></div>
                        <div className="square default"></div>
                        <div className="square"></div>
                    </div>
                    <div className="container">
                        <Field 
                            id="username" 
                            label="Username" 
                            value={this.state.form.username}
                            setFieldValue={this.setFieldValue.bind(this)}
                        />
                        <Field 
                            type="password" 
                            id="password" 
                            label="Password" 
                            value={this.state.form.password}
                            setFieldValue={this.setFieldValue.bind(this)}
                        />
                        <div className="field-group">
                            <button className="button primary" onClick={this.handleSubmit.bind(this)}>LOGIN</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

export default Login;