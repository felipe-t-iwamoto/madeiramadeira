import React from "react";
import "./system.css";

import Users from "./../Users";
import AddUser from "./../Users/AddUser";
import EditUser from "./../Users/EditUser";
import Contacts from "./../Contacts";
import AddContact from "./../Contacts/AddContact";
import EditContact from "./../Contacts/EditContact";

import { BrowserRouter, Route, Switch, Link } from 'react-router-dom';

class System extends React.Component {

    componentDidMount() {
        this.isLogged();
    }

    isLogged(){
        let logged = localStorage.getItem('logged');
        logged = JSON.parse(logged);

        if(logged !== null){
            this.props.history.push('/system');
        }else{
            this.props.history.push('/');
        }
    }

    logout(){
        localStorage.setItem("logged", null);
        this.isLogged();
    }

    render() {
        return (
            <div id="system">
                <div className="container">
                    <BrowserRouter>
                        <div className="wg grid">
                            <div className="col-md-3">
                                <div className="leftbar">
                                    <div className="user">
                                        <div className="background">

                                        </div>
                                        <div className="photo">

                                        </div>
                                    </div>
                                    <ul className="menu">
                                        <li className="item"><Link to="/system/contacts">Contacts</Link></li>
                                        <li className="item"><Link to="/system/users">Users</Link></li>
                                        <li className="item"><Link onClick={this.logout.bind(this)}>Logout</Link></li>
                                    </ul>
                                </div>
                            </div>
                            <div className="col-md-9">
                                <Switch>
                                    <Route exact path="/system/users" component={Users} />
                                    <Route exact path="/system/users/new" component={AddUser} />
                                    <Route path="/system/users/:name" component={EditUser} />
                                    <Route exact path="/system/contacts" component={Contacts} />
                                    <Route exact path="/system/contacts/new" component={AddContact} />
                                    <Route path="/system/contacts/:phone" component={EditContact} />
                                    {/* <Route path="/system/logout" component={Logout} /> */}
                                </Switch>
                            </div>
                        </div>
                    </BrowserRouter>
                </div>
            </div>
        );
    }

}

export default System;