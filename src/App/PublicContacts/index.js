import React from "react";
import "./public-contacts.css";

import PhoneValidator from "./../../Validator/PhoneValidator";

class PublicContacts extends React.Component {

    constructor(props) {

        super(props);

        this.state = {
            contacts: []
        };

    }

    componentDidMount() {
        this.setContacts(this.getContacts());
    }

    getContacts() {
        let contacts = localStorage.getItem('contacts');
        return JSON.parse(contacts);
    }

    setContacts(contacts) {
        this.setState({ contacts });
        localStorage.setItem('contacts', JSON.stringify(contacts));
    }

    render() {
        return (
            <div id="public-contacts">
                <div className="container">

                    <div className="header">
                        <div className="wg grid">
                            <div className="col-sm-6">
                                <h3 className="title">Contact List</h3>
                            </div>
                            <div className="col-sm-6"></div>
                        </div>
                    </div>
                    <div className="body">
                        <table className="table">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>E-mail</th>
                                    <th>Phone</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.contacts.length > 0 ?
                                        this.state.contacts.map((contact, index) => (
                                            !contact.isPrivate && 
                                            <tr key={index}>
                                                <td><span title={contact.name}>{contact.name}</span></td>
                                                <td><span title={contact.email}>{contact.email}</span></td>
                                                <td><span title={PhoneValidator.validator(contact.phone)}>{PhoneValidator.validator(contact.phone)}</span></td>
                                            </tr>
                                        ))
                                        :
                                        <tr>
                                            <td className="not-found" colSpan="4">No results founded.</td>
                                        </tr>
                                }
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        );
    }
}

export default PublicContacts;