import React from "react";
import "./contacts.css";

import PhoneValidator from "./../../Validator/PhoneValidator";

class Contacts extends React.Component {

    constructor(props){

        super(props);

        this.state = {
            contacts: [],
            filter: "all"
        };
    }

    componentDidMount(){
        this.setContacts(this.getContacts());
    }

    getContacts() {
        let contacts = localStorage.getItem('contacts');
        return JSON.parse(contacts);
    }

    setContacts(contacts) {
        this.setState({ contacts });
        localStorage.setItem('contacts', JSON.stringify(contacts));
    }

    removeContact(id){
        let contacts = this.getContacts().filter((contact, index) => ( index !== id));
        this.setContacts(contacts);
    }

    getFilter(contact){
        switch(this.state.filter){
            case "all": return true;
            case "private": return contact.isPrivate;
            case "public": return !contact.isPrivate;
            default: return false;
        }
    }

    setFilter(filter){
        this.setState({ filter });
    }

    render() {

        return (
            <div id="contacts">
                <div className="header">
                    <div className="wg grid">
                        <div className="col-lg-6">
                            <h3 className="title">Contact List</h3>
                        </div>
                        <div className="col-lg-6">
                            <button className={`button ${this.state.filter === "all" && `primary`}`} onClick={() => this.setFilter("all")}>All</button>
                            <button className={`button ${this.state.filter === "private" && `primary`}`} onClick={() => this.setFilter("private")}>Privates</button>
                            <button className={`button ${this.state.filter === "public" && `primary`}`} onClick={() => this.setFilter("public")}>Publics</button>
                            <button className="button" onClick={() => this.props.history.push('/system/contacts/new')}>+ ADD NEW</button>
                        </div>
                    </div>
                </div>
                <div className="body">
                    <table className="table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>E-mail</th>
                                <th>Phone</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.contacts.length > 0 ?
                                this.state.contacts.map((contact, index) => (
                                    this.getFilter(contact) &&
                                    <tr key={index}>
                                        <td><span title={contact.name}>{contact.name}</span></td>
                                        <td><span title={contact.email}>{contact.email}</span></td>
                                        <td><span title={PhoneValidator.validator(contact.phone)}>{PhoneValidator.validator(contact.phone)}</span></td>
                                        <td>
                                            <div className="actions">
                                                <button onClick={() => this.props.history.push(`/system/contacts/${contact.phone}`)}>edit</button>
                                                <button onClick={this.removeContact.bind(this, index)}>remove</button>
                                            </div>
                                        </td>
                                    </tr>
                                ))
                                :
                                <tr>
                                    <td className="not-found" colSpan="4">No results founded.</td>
                                </tr>
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default Contacts;