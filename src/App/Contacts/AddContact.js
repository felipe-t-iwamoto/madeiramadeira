import React from "react";

import Field from "../../Components/Field";
import Notify from "../../Components/Notify";

import PhoneValidator from "./../../Validator/PhoneValidator";

class AddContact extends React.Component {

    constructor(props) {

        super(props);

        this.state = {
            form: {
                name: "",
                email: "",
                phone: "",
                isPrivate: false,
            },
            errors: []
        }
    }

    getContacts() {
        let contacts = localStorage.getItem('contacts');
        return JSON.parse(contacts);
    }

    setContacts(contacts) {
        this.setState({ contacts });
        localStorage.setItem('contacts', JSON.stringify(contacts));
    }

    setFieldValue(id, value) {
        let form = this.state.form;
        form[id] = value;
        this.setState({ form });
    }

    setCheckboxValue(id) {
        let form = this.state.form;
        form[id] = this.refs[id].checked;
        this.setState({ form });
    }

    throwErrors(errors) {
        this.setState({ errors }, () => {
            setTimeout(() => {
                this.setState({ errors: [] });
            }, 5000);
        });
    }

    validate() {
        let requireds = ["name", "email", "phone", "isPrivate"];
        let errors = [];

        requireds.forEach((field) => {
            if (this.state.form[field].length < 1) {
                errors.push(`${field} field is mandatory.`)
            }
        });

        return errors;
    }

    handleSubmit() {
        let { name, email, phone, isPrivate } = this.state.form;
        let errors = this.validate();

        if (errors.length > 0) {
            return this.throwErrors(errors);
        }

        let contacts = this.getContacts();
        phone = phone.replace(/\D/g, '');
        let checker = contacts.find((contact) => (contact.phone === phone));

        if (checker) {
            errors.push(`Contact already exists.`);
            return this.throwErrors(errors);
        }

        phone = phone.replace(/\D/g, '');

        contacts.push({ name, email, phone, isPrivate });
        this.setContacts(contacts);
        this.props.history.push("/system/contacts");
    }

    render() {

        return (
            <div id="contacts">
                <Notify errors={this.state.errors} />
                <div className="header">
                    <h3 className="title">Add Contact</h3>
                </div>

                <div className="wg grid">
                    <div className="col-lg-4 col-md-6 col-sm-12">
                        <Field
                            id="name"
                            label="Full name"
                            value={this.state.form.name}
                            setFieldValue={this.setFieldValue.bind(this)}
                        />
                        <Field
                            id="email"
                            label="E-mail"
                            value={this.state.form.email}
                            setFieldValue={this.setFieldValue.bind(this)}
                        />
                        <Field
                            id="phone"
                            label="Phone"
                            value={this.state.form.phone}
                            setFieldValue={this.setFieldValue.bind(this)}
                            validator={PhoneValidator.validator}
                        />
                        <div className="field-group">
                            <label 
                                htmlFor="isPrivate" 
                                onClick={this.setCheckboxValue.bind(this, "isPrivate")}
                            >
                                <input
                                    type="checkbox"
                                    id="isPrivate"
                                    ref="isPrivate"
                                /> This is a private contact?
                            </label>
                        </div>
                        <button className="button primary" onClick={this.handleSubmit.bind(this)}>+ Add Contact</button>
                    </div>
                </div>
            </div>
        );
    }

}

export default AddContact;