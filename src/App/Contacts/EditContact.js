import React from "react";

import Field from "../../Components/Field";
import Notify from "../../Components/Notify";

import PhoneValidator from "./../../Validator/PhoneValidator";

class AddContact extends React.Component {

    constructor(props) {

        super(props);

        this.state = {
            form: {
                name: "",
                email: "",
                phone: "",
                isPrivate: false,
            },
            formReady: false,
            errors: []
        }
    }

    componentDidMount(){
        this.setForm();
    }

    setForm(){
        let contacts = this.getContacts();
        let phone = this.props.match.params.phone;
        let contact = contacts.find((contact) => (contact.phone === phone));
        let form = this.state.form;

        form.name = contact.name;
        form.email = contact.email;
        form.phone = contact.phone;
        form.isPrivate = contact.isPrivate;

        this.setState({ form }, () => {
            this.setState({ formReady: true })
        });
    }

    getContacts() {
        let contacts = localStorage.getItem('contacts');
        return JSON.parse(contacts);
    }

    setContacts(contacts) {
        this.setState({ contacts });
        localStorage.setItem('contacts', JSON.stringify(contacts));
    }

    setFieldValue(id, value) {
        let form = this.state.form;
        form[id] = value;
        this.setState({ form });
    }

    setCheckboxValue(id) {
        let form = this.state.form;
        form[id] = this.refs[id].checked;
        this.setState({ form });
    }

    throwErrors(errors) {
        this.setState({ errors }, () => {
            setTimeout(() => {
                this.setState({ errors: [] });
            }, 5000);
        });
    }

    validate() {
        let requireds = ["name", "email", "phone", "isPrivate"];
        let errors = [];

        requireds.forEach((field) => {
            if (this.state.form[field].length < 1) {
                errors.push(`${field} field is mandatory.`)
            }
        });

        return errors;
    }

    handleSubmit() {
        let { name, email, phone, isPrivate } = this.state.form;
        let errors = this.validate();

        if (errors.length > 0) {
            return this.throwErrors(errors);
        }

        let phoneParam = this.props.match.params.phone;
        let contacts = this.getContacts();
        let checker = contacts.find((contact) => (contact.phone === phone && (phoneParam !== phone)));

        if (checker) {
            errors.push(`Contact already exists.`);
            return this.throwErrors(errors);
        }

        contacts = contacts.map((contact, index) => {
            if(contact.phone === phoneParam){
                contact.name = name;
                contact.email = email;
                contact.phone = phone.replace(/\D/g, '');
                contact.isPrivate = isPrivate;
            }

            return contact;
        });

        this.setContacts(contacts);
        this.props.history.push("/system/contacts");
    }

    render() {

        return (
            <div id="contacts">
                <Notify errors={this.state.errors} />
                <div className="header">
                    <h3 className="title">Edit Contact</h3>
                </div>

                <div className="wg grid">
                    {
                        this.state.formReady &&
                        <div className="col-lg-4 col-md-6 col-sm-12">
                        <Field
                            id="name"
                            label="Full name"
                            value={this.state.form.name}
                            setFieldValue={this.setFieldValue.bind(this)}
                        />
                        <Field
                            id="email"
                            label="E-mail"
                            value={this.state.form.email}
                            setFieldValue={this.setFieldValue.bind(this)}
                        />
                        <Field
                            id="phone"
                            label="Phone"
                            value={this.state.form.phone}
                            setFieldValue={this.setFieldValue.bind(this)}
                            validator={PhoneValidator.validator}
                        />
                        <div className="field-group">
                            <label htmlFor="isPrivate">
                                <input
                                    type="checkbox"
                                    id="isPrivate"
                                    ref="isPrivate"
                                    checked={this.state.form.isPrivate}
                                    onChange={this.setCheckboxValue.bind(this, "isPrivate")}
                                /> This is a private contact?
                            </label>
                        </div>
                        <button className="button primary" onClick={this.handleSubmit.bind(this)}>Edit Contact</button>
                    </div>
                    }
                </div>
            </div>
        );
    }

}

export default AddContact;