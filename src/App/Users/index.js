import React from "react";
import "./users.css";

class Users extends React.Component {

    constructor(props){

        super(props);

        this.state = {
            users: []
        };
    }

    componentDidMount(){
        this.setUsers(this.getUsers());
    }

    getUsers() {
        let users = localStorage.getItem('users');
        return JSON.parse(users);
    }

    setUsers(users) {
        this.setState({ users });
        localStorage.setItem('users', JSON.stringify(users));
    }

    removeUser(id){
        let users = this.getUsers().filter((user, index) => ( index !== id));
        this.setUsers(users);
    }

    render() {

        return (
            <div id="users">
                <div className="header">
                    <div className="wg grid">
                        <div className="col-sm-6">
                            <h3 className="title">User List</h3>
                        </div>
                        <div className="col-sm-3">
                            <button className="button" onClick={() => this.props.history.push('/system/users/new')}>+ ADD NEW</button>
                        </div>
                    </div>
                </div>
                <div className="body">
                    <table className="table">
                        <thead>
                            <tr>
                                <th>Username</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.users.length > 0 ?
                                this.state.users.map((user, index) => (
                                    <tr key={index}>
                                        <td><span title={user.username}>{user.username}</span></td>
                                        <td>
                                            <div className="actions">
                                                <button onClick={() => this.props.history.push(`/system/users/${user.username}`)}>edit</button>
                                                <button onClick={this.removeUser.bind(this, index)}>remove</button>
                                            </div>
                                        </td>
                                    </tr>
                                ))
                                :
                                <tr>
                                    <td className="not-found" colSpan="2">No results founded.</td>
                                </tr>
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default Users;