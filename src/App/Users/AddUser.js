import React from "react";

import Field from "./../../Components/Field";
import Notify from "./../../Components/Notify";

class AddUser extends React.Component {

    constructor(props){

        super(props);

        this.state = {
            form: {
                username: "",
                password: "",
            },
            errors: []
        }
    }

    getUsers() {
        let users = localStorage.getItem('users');
        return JSON.parse(users);
    }

    setUsers(users) {
        this.setState({ users });
        localStorage.setItem('users', JSON.stringify(users));
    }
    
    setFieldValue(id, value){
        let form = this.state.form;
        form[id] = value;
        this.setState({ form });
    }

    throwErrors(errors) {
        this.setState({ errors }, () => {
            setTimeout(() => {
                this.setState({ errors: [] });
            }, 5000);
        });
    }

    handleSubmit() {
        let { username, password } = this.state.form;
        let errors = [];

        if (username.length < 1) {
            errors.push(`Username field is mandatory`);
        }

        if (password.length < 1) {
            errors.push(`Password field is mandatory`);
        }

        if (errors.length > 0) {
            return this.throwErrors(errors);
        }

        let users = this.getUsers();
        let checker = users.find((user) => (user.username === username));

        if(checker){
            errors.push(`User already exists.`);
            return this.throwErrors(errors);
        }

        users.push({ username, password });
        this.setUsers(users);
        this.props.history.push("/system/users");
    }

    render() {

        return (
            <div id="users">
                <Notify errors={this.state.errors} />
                <div className="header">
                    <h3 className="title">Add User</h3>
                </div>

                <div className="wg grid">
                    <div className="col-lg-4 col-md-6 col-sm-12">
                        <Field
                            id="username"
                            label="Username"
                            value={this.state.form.username}
                            setFieldValue={this.setFieldValue.bind(this)}
                        />
                        <Field
                            type="password"
                            id="password"
                            label="Password"
                            value={this.state.form.password}
                            setFieldValue={this.setFieldValue.bind(this)}
                        />
                        <button className="button primary" onClick={this.handleSubmit.bind(this)}>+ Add User</button>
                    </div>
                </div>
            </div>
        );
    }

}

export default AddUser;