import React from "react";

import Field from "./../../Components/Field";
import Notify from "./../../Components/Notify";

class EditUser extends React.Component {

    constructor(props){

        super(props);

        this.state = {
            form: {
                username: "",
            },
            errors: []
        }
    }

    componentDidMount(){
        this.setForm();
    }

    setForm(){
        let users = this.getUsers();
        let username = this.props.match.params.name;
        let user = users.find((user) => (user.username === username));
        let form = this.state.form;

        form.username = user.username;

        this.setState({ form });
    }

    getUsers() {
        let users = localStorage.getItem('users');
        return JSON.parse(users);
    }

    setUsers(users) {
        this.setState({ users });
        localStorage.setItem('users', JSON.stringify(users));
    }
    
    setFieldValue(id, value){
        let form = this.state.form;
        form[id] = value;
        this.setState({ form });
    }

    throwErrors(errors) {
        this.setState({ errors }, () => {
            setTimeout(() => {
                this.setState({ errors: [] });
            }, 5000);
        });
    }

    handleSubmit() {
        let username = this.state.form.username;
        let errors = [];

        if (username.length < 1) {
            errors.push(`Username field is mandatory.`);
        }

        if (errors.length > 0) {
            return this.throwErrors(errors);
        }

        let usernameParam = this.props.match.params.name;
        let users = this.getUsers();
        let checker = users.find((user) => (user.username === username && (usernameParam !== username)));

        if(checker){
            errors.push(`User already exists.`);
            return this.throwErrors(errors);
        }

        users = users.map((user, index) => {
            if(user.username === usernameParam){
                user.username = username;
            }

            return user;
        });

        this.setUsers(users);
        this.props.history.push("/system/users");
    }

    render() {

        return (
            <div id="users">
                <Notify errors={this.state.errors} />
                <div className="header">
                    <h3 className="title">Edit User</h3>
                </div>

                <div className="wg grid">
                    <div className="col-lg-4 col-md-6 col-sm-12">
                        <Field
                            id="username"
                            label="Username"
                            value={this.state.form.username}
                            setFieldValue={this.setFieldValue.bind(this)}
                        />
                        <button className="button primary" onClick={this.handleSubmit.bind(this)}>Edit User</button>
                    </div>
                </div>
            </div>
        );
    }

}

export default EditUser;