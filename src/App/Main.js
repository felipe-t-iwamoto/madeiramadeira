import React from "react";
import "./main.css";

import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Login from "./Login";
import PublicContacts from "./PublicContacts";
import System from "./System";

class Main extends React.Component {

    componentDidMount(){
        this.initUsers();
        this.initContacts();
    }

    initUsers() {
        let users = localStorage.getItem('users');

        if((JSON.parse(users) && JSON.parse(users).length < 1) || JSON.parse(users) === null){
            users = [
                { username:"admin", password: "admin" },
            ];
    
            localStorage.setItem('users', JSON.stringify(users));
        }
    }
    
    initContacts() {
        let contacts = localStorage.getItem('contacts');

        if((JSON.parse(contacts) && JSON.parse(contacts).length < 1) || JSON.parse(contacts) === null){
            localStorage.setItem('contacts', JSON.stringify([]));
        }
    }

    render() {
        return (
            <div id="main">
                <BrowserRouter>
                    <Switch>
                        <Route exact path="/" component={Login} />
                        <Route exact path="/contacts" component={PublicContacts} />
                        <Route path="/system" component={System} />
                    </Switch>
                </BrowserRouter>
            </div>
        );
    }

}

export default Main;