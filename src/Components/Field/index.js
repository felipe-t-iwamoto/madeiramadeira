import React from "react";
import "./field-group.css";

class Field extends React.Component {

    componentDidMount(){
        this.handleChange();
    }

    handleChange(){
        let value = this.refs[this.props.id].value;

        if(this.props.validator !== null){
            
            value = this.props.validator.call(this, value);
        }

        this.props.setFieldValue(this.props.id, value);
    }
    
    render() {
        return (
            <div className="field-group">
                <input 
                    type={`${this.props.type}`} 
                    className="field" 
                    ref={`${this.props.id}`} 
                    id={`${this.props.id}`} 
                    name={`${this.props.id}`} 
                    value={`${this.props.value}`}
                    placeholder={`${this.props.placeholder}`}
                    onChange={this.handleChange.bind(this)}
                />
                <label htmlFor={`${this.props.id}`}>{this.props.label}</label>
            </div>
        );
    }

}

Field.defaultProps = {
    type: `text`,
    id: ``,
    label: ``,
    placeholder: ``,
    value: ``,
    validator: null
}

export default Field;